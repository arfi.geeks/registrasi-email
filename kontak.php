<?php include "koneksi/koneksi.php";?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>KONTAK</title>

    <!-- Favicon -->
    <link rel="shortcut icon" href="assets/img/favicon.png">

    <!-- Bootstrap -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- Style -->
    <link href="assets/css/style.css" rel="stylesheet">
    <link href="assets/css/responsive.css" rel="stylesheet">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="assets/css/font-awesome.min.css">

    <!-- Font Google -->
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,900italic,900,700italic,700,500italic,500,400italic,300italic,300,100italic,100' rel='stylesheet' type='text/css'>

    <!-- Hover CSS -->
    <link href="assets/css/hover.css" rel="stylesheet" media="all">

    <!-- Slick Banner -->
    <link rel="stylesheet" type="text/css" href="assets/js/slick/slick.css"/>
    <link rel="stylesheet" type="text/css" href="assets/js/slick/slick-theme.css"/>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>
    <div class="main-menu default" id="navigation">
      <nav class="navbar navbar-default">
        <div class="container">
          <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.php"><img alt="logo" src="assets/img/logo-1.png" width="200px"></a>
          </div>

          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
              <!--<li><a href="index.html"><i aria-hidden="true" class="fa fa-home"></i> Beranda</a></li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i aria-hidden="true" class="fa fa-file-text-o"></i> Profil <span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><a href="#">Struktur Organisasi</a></li>
                  <li><a href="#">Visi & Misi</a></li>
                </ul>
              </li>
              <li><a href="#"><i aria-hidden="true" class="fa fa-newspaper-o"></i> Publikasi</a></li>
              <li><a href="#"><i aria-hidden="true" class="fa fa-phone"></i> Kontak</a></li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i aria-hidden="true" class="fa fa-photo"></i> Dokumentasi <span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><a href="#">Foto</a></li>
                  <li><a href="#">Video</a></li>
                </ul>
              </li>-->

              <li class="active"><a href="index.php">Beranda</a></li>
              <li><a href="registrasi.php">Registrasi</a></li>
              <li><a href="kontak.php">Kontak</a></li>
            </ul>
          </div>
        </div>
      </nav>
    </div>

    <div class="main-banner">
      <img src="assets/img/banner/kontak.png" class="img-responsive">
    </div>

    <div class="main-content">
      <div class="container">
        <div class="row">
          <div class="col-lg-6 col-lg-offset-3 mrgn-btm20">
            <div class="line-dropdown">
              <h4><span>Kontak</span></h4>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-6">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3965.1962923730835!2d106.83125141476985!3d-6.368638195391543!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69ec0846d18769%3A0x87eb6f64d1a8a3b2!2sGunadarma%20University%20Campus%20D!5e0!3m2!1sen!2sid!4v1577689062065!5m2!1sen!2sid" width="100%" height="350" frameborder="0" style="border:0" allowfullscreen></iframe>
          </div>
          <div class="col-lg-6">
            <div class="kontak">
              <div class="form-group">
                <p><i aria-hidden="true" class="fa fa-phone"></i> +62 - 21 - 78881112 ext. 234</p>
                <p><i aria-hidden="true" class="fa fa-envelope-o"></i> mediacenter [@] gunadarma.ac.id </p>
                <p class="mrgn-btm30"><i aria-hidden="true" class="fa fa-map-marker"></i> Jl. Margonda Raya 100, Depok West Java, INDONESIA - 16424</p>
              </div>
              <div class="form-group">
                <p>Jika anda memiliki pertanyaan/saran/kritik silahkan mengisi kolom dibawah ini :</p>
              </div>
              <form action="" method="post">
                <div class="form-group">
                <div class="form-group">
                  <label>Nama</label>
                  <input type="text" name="nama" class="form-control" required="">
                </div>
                  <label>Email</label>
                  <input type="email" name="email" class="form-control" required="">
                </div>
                <div class="form-group">
                  <label>Pesan</label>
                  <textarea class="form-control" name="pesan" rows="5" required=""></textarea>
                </div>
                <div class="form-group">
                  <button class="btn btn-default" name="submit" type="submit">Kirim</button>
                  <?php include "crud/crud_kontak.php";?>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>


    <div class="footer">
      <div class="container">
        <div class="row">
          <div class="col-lg-8 col-lg-offset-2">
            <center><img alt="logo" src="assets/img/logo-1.png" class="img-responsive" width="200px"></center>
            <h5><i aria-hidden="true" class="fa fa-map-marker"></i>Jl. Margonda Raya 100, Depok West Java, INDONESIA - 16424<br><i aria-hidden="true" class="fa fa-envelope-o"></i> mediacenter [@] gunadarma.ac.id | <i aria-hidden="true" class="fa fa-phone"></i> +62 - 21 - 78881112 ext. 234</h5>
            <p>Hak Cipta &copy; 2019. UNIVERSITAS GUNADARMA</p>
          </div>
        </div>
      </div>
    </div>
    
    <a href="#" class="back-to-top"><span id="toTopHover" style="opacity: 1;"> </span></a>
    
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="assets/js/bootstrap.min.js"></script>

    <!-- Slick Banner -->
    <script type="text/javascript" src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
    <script type="text/javascript" src="assets/js/slick/slick.min.js"></script>

    <!-- Photo & Video Popup -->
    <script type="text/javascript" src="assets/js/jquery.fancybox.js?v=2.1.5"></script>
    <link rel="stylesheet" type="text/css" href="assets/css/jquery.fancybox.css?v=2.1.5" media="screen" />
    <script type="text/javascript" src="assets/js/jquery.fancybox-media.js?v=1.0.6"></script>
    <script type="text/javascript">
      $(document).ready(function() {
        $('.fancybox').fancybox();
        $('.fancybox-media')
          .attr('rel', 'media-gallery')
          .fancybox({
            openEffect : 'none',
            closeEffect : 'none',
            prevEffect : 'none',
            nextEffect : 'none',
            arrows : false,
            helpers : {
            media : {},
            buttons : {}
          }
        });
      });
    </script>

    <!-- Own Js -->
    <script type="text/javascript" src="assets/js/own.js"></script>

    <!-- Top Fixed Menu -->
    <script type="text/javascript" src="assets/js/nagging-menu.js" charset="utf-8"></script>
  </body>
</html>