-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Dec 30, 2019 at 06:28 PM
-- Server version: 5.7.26
-- PHP Version: 7.3.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `student`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `admin_id` int(11) NOT NULL,
  `user` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`admin_id`, `user`, `password`) VALUES
(1, 'admin', '0192023a7bbd73250516f069df18b500'),
(2, 'user', 'ee11cbb19052e40b07aac0ca060c23ee');

-- --------------------------------------------------------

--
-- Table structure for table `pesan`
--

CREATE TABLE `pesan` (
  `pesan_id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `pesan` longtext NOT NULL,
  `tgl` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pesan`
--

INSERT INTO `pesan` (`pesan_id`, `nama`, `email`, `pesan`, `tgl`) VALUES
(1, 'Arfi Yanto Susilo', 'arfiyantosusilo@gmail.com', 'afa', '2016-09-21'),
(2, 'Arfi', 'arfi@gmail.com', 'Test', '2019-12-30');

-- --------------------------------------------------------

--
-- Table structure for table `registrasi`
--

CREATE TABLE `registrasi` (
  `registrasi_id` int(11) NOT NULL,
  `nim` varchar(25) DEFAULT NULL,
  `nama_lengkap` varchar(50) DEFAULT NULL,
  `tgl_lahir` varchar(50) DEFAULT NULL,
  `fakultas` varchar(50) DEFAULT NULL,
  `request_email` varchar(100) DEFAULT NULL,
  `nomor_kontak` varchar(50) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `alamat` longtext,
  `status_mahasiswa` varchar(50) DEFAULT NULL,
  `tgl_request` datetime DEFAULT NULL,
  `status_pendaftaran` varchar(20) DEFAULT NULL,
  `keterangan_status` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `registrasi`
--

INSERT INTO `registrasi` (`registrasi_id`, `nim`, `nama_lengkap`, `tgl_lahir`, `fakultas`, `request_email`, `nomor_kontak`, `email`, `alamat`, `status_mahasiswa`, `tgl_request`, `status_pendaftaran`, `keterangan_status`) VALUES
(5, '11116024', 'Arfi Yanto Susilo', '2019-12-23', 'S1 Sistem Informasi', 'arfi@gunadarma.ac.id', '081289491885', 'arfiyantosusilo@gmail.com', 'Jakarta', 'Mahasiswa Aktif', '2019-12-30 18:24:16', 'Berhasil', NULL),
(6, '11118571', 'Muhammad Aris', '2019-12-30', 'S1 Sistem Informasi', 'aris@student.gunadarma.ac.id', '0812746781', 'aris@gmail.com', 'Jakarta', 'Mahasiswa Aktif', '2019-12-30 18:25:17', 'Tidak Berhasil', 'Mohon isi form dengan benar');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `pesan`
--
ALTER TABLE `pesan`
  ADD PRIMARY KEY (`pesan_id`);

--
-- Indexes for table `registrasi`
--
ALTER TABLE `registrasi`
  ADD PRIMARY KEY (`registrasi_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `admin_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `pesan`
--
ALTER TABLE `pesan`
  MODIFY `pesan_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `registrasi`
--
ALTER TABLE `registrasi`
  MODIFY `registrasi_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
