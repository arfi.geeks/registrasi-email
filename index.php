<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>BERANDA</title>

    <!-- Favicon -->
    <link rel="shortcut icon" href="assets/img/favicon.png">

    <!-- Bootstrap -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- Style -->
    <link href="assets/css/style.css" rel="stylesheet">
    <link href="assets/css/responsive.css" rel="stylesheet">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="assets/css/font-awesome.min.css">

    <!-- Font Google -->
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,900italic,900,700italic,700,500italic,500,400italic,300italic,300,100italic,100' rel='stylesheet' type='text/css'>

    <!-- Hover CSS -->
    <link href="assets/css/hover.css" rel="stylesheet" media="all">

    <!-- Banner Animation Home -->
    <link rel="stylesheet" href="assets/css/animation.css" type="text/css" />

    <!-- Slick Banner -->
    <link rel="stylesheet" type="text/css" href="assets/js/slick/slick.css"/>
    <link rel="stylesheet" type="text/css" href="assets/js/slick/slick-theme.css"/>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>
    <div class="main-menu default" id="navigation">
      <nav class="navbar navbar-default">
        <div class="container">
          <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.php"><img alt="logo" src="assets/img/logo-1.png" width="200px"></a>
          </div>

          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
              <!--<li><a href="index.html"><i aria-hidden="true" class="fa fa-home"></i> Beranda</a></li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i aria-hidden="true" class="fa fa-file-text-o"></i> Profil <span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><a href="#">Struktur Organisasi</a></li>
                  <li><a href="#">Visi & Misi</a></li>
                </ul>
              </li>
              <li><a href="#"><i aria-hidden="true" class="fa fa-newspaper-o"></i> Publikasi</a></li>
              <li><a href="#"><i aria-hidden="true" class="fa fa-phone"></i> Kontak</a></li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i aria-hidden="true" class="fa fa-photo"></i> Dokumentasi <span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><a href="#">Foto</a></li>
                  <li><a href="#">Video</a></li>
                </ul>
              </li>-->

              <li class="active"><a href="index.php">Beranda</a></li>
              <li><a href="registrasi.php">Registrasi</a></li>
              <li><a href="kontak.php">Kontak</a></li>
            </ul>
          </div>
        </div>
      </nav>
    </div>

    <div class="banner">
      <div id="layerslider" style="width: 1920px; height: 697px;">

		 <div class="ls-slide" data-ls="slidedelay: 5000; transition2d: 2,7,9;">
 				<img src="assets/img/bgslider4.png" class="ls-bg" alt="Slide background">\
  				<img class="ls-l" style="top:480px;left:620px;white-space: nowrap;" data-ls="offsetxin:0;offsetyin:bottom;durationin:1000;easingin:easeOutQuad;fadein:false;rotatein:-10;offsetxout:0;durationout:1500;" src="assets/img/tong.png" alt="">
  				<img class="ls-l" style="top:180px;left:590px;white-space: nowrap;" data-ls="offsetxin:0;offsetyin:top;durationin:1500;delayin:1600;easingin:easeInOutQuart;fadein:false;scalexin:5;scaleyin:5;offsetxout:0;offsetyout:top;durationout:1000;fadeout:false;" src="assets/img/text-slider402.png" alt="">   
					<img class="ls-l" style="top:230px;left:380px;white-space: nowrap;" data-ls="offsetxin:0;durationin:1500;delayin:1500;rotateyin:90;transformoriginin:right 50% 0;offsetxout:0;durationout:1500;showuntil:5000;rotateyout:-90;transformoriginout:right 50% 0;" src="assets/img/laki-1.png" alt="">
				<img class="ls-l" style="top:230px;left:950px;white-space: nowrap;"  data-ls="offsetxin:0;durationin:1500;delayin:1500;rotateyin:90;transformoriginin:right 50% 0;offsetxout:0;durationout:1500;showuntil:5000;rotateyout:-90;transformoriginout:right 50% 0;" src="assets/img/ibu.png" alt="">
		   </div>

		   <div class="ls-slide" data-ls="slidedelay: 5000; transition2d: 2,7,9;">
 				<img src="assets/img/bg-slide-1.png" class="ls-bg" >
			 	<img class="ls-l" style="top:285px;left:310px;white-space: nowrap;" data-ls="offsetxin:left;durationin:2000;delayin:1000;fadein:false;offsetxout:left;durationout:1000;fadeout:false;" src="assets/img/orang3.png" alt="">
				<img class="ls-l" style="top:229px;left:670px;white-space: nowrap;" data-ls="offsetxin:left;durationin:1500;delayin:1000;fadein:false;offsetxout:left;durationout:1000;fadeout:false;" src="assets/img/orang2.png" alt="">
  				<img class="ls-l" style="top:169px;left:800px;white-space: nowrap;" data-ls="offsetxin:0;offsetyin:bottom;durationin:1000;easingin:easeOutQuad;fadein:false;rotatein:-10;offsetxout:0;durationout:1500;" src="assets/img/orang.png" alt="">
  				<img class="ls-l" style="top:175px;left:1070px;white-space: nowrap;" data-ls="offsetxin:0;offsetyin:top;durationin:1500;delayin:1600;easingin:easeInOutQuart;fadein:false;scalexin:5;scaleyin:5;offsetxout:0;offsetyout:top;durationout:1000;fadeout:false;" src="assets/img/text02new.png" alt="">   
		   </div>

			<div class="ls-slide" data-ls="slidedelay: 5000; transition2d: 2,7,9;">
 				<img src="assets/img/bg-slide2n.png" class="ls-bg" alt="Slide background">
			 	<img class="ls-l" style="top:210px;left:310px;white-space: nowrap;" data-ls="offsetxin:right;durationin:2000;delayin:1000;fadein:false;offsetxout:left;durationout:1000;fadeout:false;" src="assets/img/perahu2.png" alt="">
  				<img class="ls-l" style="top:477px;left:0px;white-space: nowrap;" data-ls="offsetxin:0;offsetyin:bottom;durationin:1000;easingin:easeOutQuad;fadein:false;rotatein:-10;offsetxout:0;durationout:1500;" src="assets/img/ombak.png" alt="">
  				<img class="ls-l" style="top:110px;left:1150px;white-space: nowrap;" data-ls="offsetxin:0;offsetyin:top;durationin:1500;delayin:1600;easingin:easeInOutQuart;fadein:false;scalexin:5;scaleyin:5;offsetxout:0;offsetyout:top;durationout:1000;fadeout:false;" src="assets/img/text-slider2-new.png" alt="">   
		   </div>


		   <div class="ls-slide" data-ls="slidedelay: 5000; transition2d: 2,7,9;">
 				<img src="assets/img/bg-slider-3.png" class="ls-bg" alt="Slide background">\
  				<img class="ls-l" style="top:105px;left:180px;white-space: nowrap;" data-ls="offsetxin:0;offsetyin:bottom;durationin:1000;easingin:easeOutQuad;fadein:false;rotatein:-10;offsetxout:0;durationout:1500;" src="assets/img/kota.png" alt="">
  				<img class="ls-l" style="top:140px;left:900px;white-space: nowrap;" data-ls="offsetxin:0;offsetyin:top;durationin:1500;delayin:1600;easingin:easeInOutQuart;fadein:false;scalexin:5;scaleyin:5;offsetxout:0;offsetyout:top;durationout:1000;fadeout:false;" src="assets/img/text-slider-302.png" alt="">   
					<img class="ls-l" style="top:360px;left:480px;white-space: nowrap;" data-ls="offsetxin:0;durationin:3000;delayin:1000;easingin:easeOutElastic;rotatexin:90;transformoriginin:50% bottom 0;offsetxout:0;rotatexout:90;transformoriginout:50% bottom 0;" src="assets/img/mobil1.png" alt="">
				<img class="ls-l" style="top:400px;left:850px;white-space: nowrap;" data-ls="offsetxin:0;durationin:3000;delayin:1500;easingin:easeOutElastic;rotatexin:90;transformoriginin:50% bottom 0;offsetxout:0;rotatexout:90;transformoriginout:50% bottom 0;" src="assets/img/vespa.png" alt="">
				<img class="ls-l" style="top:440px;left:1130px;white-space: nowrap;" data-ls="offsetxin:0;durationin:3000;delayin:2000;easingin:easeOutElastic;rotatexin:90;transformoriginin:50% bottom 0;offsetxout:0;rotatexout:90;transformoriginout:50% bottom 0;" src="assets/img/mobil2.png" alt="">
				<img class="ls-l" style="top:380px;left:990px;white-space: nowrap;" data-ls="offsetxin:0;durationin:3000;delayin:2500;easingin:easeOutElastic;rotatexin:90;transformoriginin:50% bottom 0;offsetxout:0;rotatexout:90;transformoriginout:50% bottom 0;" src="assets/img/bajaj.png" alt="">
		   </div>



		  <div class="ls-slide" data-ls="slidedelay: 5000; transition2d: 2,7,9;">
 				<img src="assets/img/bg-slider-5.png" class="ls-bg" alt="Slide background">\
  				<img class="ls-l" style="top:385px;left:360px;white-space: nowrap;" data-ls="offsetxin:0;offsetyin:bottom;durationin:1000;easingin:easeOutQuad;fadein:false;rotatein:-10;offsetxout:0;durationout:1500;" src="assets/img/bayangan-orang.png" alt="">
  				<img class="ls-l" style="top:180px;left:1200px;white-space: nowrap;" data-ls="offsetxin:0;offsetyin:top;durationin:1500;delayin:1600;easingin:easeInOutQuart;fadein:false;scalexin:5;scaleyin:5;offsetxout:0;offsetyout:top;durationout:1000;fadeout:false;" src="assets/img/text-slider-5new.png" alt="">   
					<img class="ls-l" style="top:230px;left:1000px;white-space: nowrap;" data-ls="offsetxin:-100;delayin:2500;easingin:easeInOutCubic;rotatein:-180;offsetxout:300;durationout:1000;easingout:easeInOutCubic;rotateout:360;" src="assets/img/laki-ijo.png" alt="">
				<img class="ls-l" style="top:310px;left:800px;white-space: nowrap;" data-ls="offsetxin:-100;delayin:2500;easingin:easeInOutCubic;rotatein:-180;offsetxout:300;durationout:1000;easingout:easeInOutCubic;rotateout:360;"  src="assets/img/ibu-pink.png" alt="">
		   </div>

		   <div class="ls-slide" data-ls="slidedelay: 5000; transition2d: 2,7,9;">
 				<img src="assets/img/bg-slider-6.png" class="ls-bg" alt="Slide background">
  				<img class="ls-l" style="top:175px;left:470px;white-space: nowrap;" data-ls="offsetxin:0;offsetyin:top;durationin:1500;delayin:100;easingin:easeInOutQuart;fadein:false;scalexin:5;scaleyin:5;offsetxout:0;offsetyout:top;durationout:1000;fadeout:false;" src="assets/img/lingkaran.png" alt=""> 
				<img class="ls-l" style="top:200px;left:585px;white-space: nowrap;" data-ls="offsetxin:left;durationin:1500;delayin:1000;fadein:false;offsetxout:left;durationout:1000;fadeout:false;" src="assets/img/ber4.png" alt="">  
				<img class="ls-l" style="top:200px;left:585px;white-space: nowrap;" data-ls="offsetxin:left;durationin:1500;delayin:1000;fadein:false;offsetxout:left;durationout:1000;fadeout:false;" src="assets/img/ber4.png" alt="">
				<img class="ls-l" style="top:500px;left:585px;white-space: nowrap;" data-ls="offsetxin:0;offsetyin:bottom;durationin:1000;easingin:easeOutQuad;fadein:false;rotatein:-10;offsetxout:0;durationout:1000;" src="assets/img/prinsip02.png" alt=""> 
		   </div>


		</div>
    </div>

    

    <div class="footer">
      <div class="container">
        <div class="row">
          <div class="col-lg-8 col-lg-offset-2">
            <center><img alt="logo" src="assets/img/logo-1.png" class="img-responsive" width="200px"></center>
            <h5><i aria-hidden="true" class="fa fa-map-marker"></i>Jl. Margonda Raya 100, Depok West Java, INDONESIA - 16424<br><i aria-hidden="true" class="fa fa-envelope-o"></i> mediacenter [@] gunadarma.ac.id | <i aria-hidden="true" class="fa fa-phone"></i> +62 - 21 - 78881112 ext. 234</h5>
            <p>Hak Cipta &copy; 2019. UNIVERSITAS GUNADARMA</p>
          </div>
        </div>
      </div>
    </div>
    
    
    <a href="#" class="back-to-top"><span id="toTopHover" style="opacity: 1;"> </span></a>
    
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="assets/js/bootstrap.min.js"></script>

    <!-- Slick Banner -->
    <script type="text/javascript" src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
    <script type="text/javascript" src="assets/js/slick/slick.min.js"></script>

    <!-- Photo & Video Popup -->
    <script type="text/javascript" src="assets/js/jquery.fancybox.js?v=2.1.5"></script>
    <link rel="stylesheet" type="text/css" href="assets/css/jquery.fancybox.css?v=2.1.5" media="screen" />
    <script type="text/javascript" src="assets/js/jquery.fancybox-media.js?v=1.0.6"></script>
    <script type="text/javascript">
      $(document).ready(function() {
        $('.fancybox').fancybox();
        $('.fancybox-media')
          .attr('rel', 'media-gallery')
          .fancybox({
            openEffect : 'none',
            closeEffect : 'none',
            prevEffect : 'none',
            nextEffect : 'none',
            arrows : false,
            helpers : {
            media : {},
            buttons : {}
          }
        });
      });
    </script>

    <!-- Own Js -->
    <script type="text/javascript" src="assets/js/own.js"></script>

    <!-- Top Fixed Menu -->
    <script type="text/javascript" src="assets/js/nagging-menu.js" charset="utf-8"></script>

    <!-- Banner Animation Home -->
    <script type="text/javascript" src="assets/js/animation.js"></script>
  </body>
</html>