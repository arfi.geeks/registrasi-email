<?php session_start(); 
include "koneksi/koneksi.php";?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>ADMINISTRATOR</title>

    <!-- Favicon -->
    <link rel="shortcut icon" href="assets/img/favicon.jpg">

    <!-- Bootstrap -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- Style -->
    <link href="assets/css/style.css" rel="stylesheet">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="assets/css/font-awesome.min.css">

    <!-- Font Google -->
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,900italic,900,700italic,700,500italic,500,400italic,300italic,300,100italic,100' rel='stylesheet' type='text/css'>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>
    <div class="top mrgn-top40">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <br>
            <center><img src="assets/img/logo-1.png" class="img-responsive" width="200"></center>
          </div>
        </div>
      </div>
    </div>

    <div class="login-area mrgn-top20">
      <div class="container">
        <div class="row">
          <div class="col-lg-4 col-lg-offset-4">
            <div class="login">
              <form action="" method="post">
                <center><h4>Member Login</h4></center>
                <label>Username</label>
                <input type="text" class="form-control mrgn-btm10" placeholder="" aria-describedby="basic-addon1" name="username">
                <label>Password</label>
                <input type="password" class="form-control mrgn-btm20" placeholder="" aria-describedby="basic-addon1" name="password">
                <center><button class="btn btn-success" type="submit" name="login">Login</button></center>
                <?php include "ceklogin.php";?>
              </form>
            </div>
            <div class="clr"></div>
          </div>
        </div>
      </div>
    </div>

    <br><br>

    <div class="footer">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <p>Hak Cipta &copy; 2019. UNIVERSITAS GUNADARMA</p>
          </div>
        </div>
      </div>
    </div>
    
    
    <a href="#" class="back-to-top"><span id="toTopHover" style="opacity: 1;"> </span></a>
    
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="assets/js/bootstrap.min.js"></script>

    <!-- Own Js -->
    <script type="text/javascript" src="assets/js/own.js"></script>
  </body>
</html>