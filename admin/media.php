<?php session_start();
if(empty($_SESSION['user'])){
}
include "koneksi/koneksi.php";?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>ADMINISTRATOR</title>

    <!-- Favicon -->
    <link rel="shortcut icon" href="assets/img/favicon.png">

    <!-- Bootstrap -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- Style -->
    <link href="assets/css/style.css" rel="stylesheet">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="assets/css/font-awesome.min.css">

    <!-- Font Google -->
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,900italic,900,700italic,700,500italic,500,400italic,300italic,300,100italic,100' rel='stylesheet' type='text/css'>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>
    <div class="top-menu">
      <nav class="navbar navbar-default">
        <div class="container">
          <div class="navbar-header">
            <a class="navbar-brand" href="media.php?hal=home"><img alt="logo" src="assets/img/logo-1.png" width="200"></a>
          </div>

          <ul class="nav navbar-nav navbar-right">
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i aria-hidden="true" class="fa fa-user"></i> <?=$_SESSION['user'];?><span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="index.php?logout">Logout</a></li>
              </ul>
            </li>
          </ul>
        </div>
      </nav>
    </div>

    <div class="main-menu">
      <div class="navbar-default">
        <div class="container">
          <div class="row">
            <div class="col-lg-12 ">
              <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                </button>
              </div>
              <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                  <li><a href="media.php?hal=home"><i aria-hidden="true" class="fa fa-dashboard"></i>Beranda</a></li>
                  <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i aria-hidden="true" class="fa fa-file-text-o"></i> Data Email <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                      <li><a href="media.php?hal=request-email">Request Email</a></li>
                      <li><a href="media.php?hal=email-di-proses">Email Sudah di Proses</a></li>
                      <li><a href="media.php?hal=email-di-tolak">Email di Tolak</a></li>
                    </ul>
                  </li>
                  <li><a href="media.php?hal=pesan"><i aria-hidden="true" class="fa fa-envelope-o"></i> Pesan</a></li>
                  <li><a href="media.php?hal=admin"><i aria-hidden="true" class="fa fa-user"></i> Administrator</a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <?php
    if(empty($_GET['hal'])){
      include "halaman/home/home.php";
    }elseif($_GET['hal'] == 'home'){
      include "halaman/home/home.php";
    }

    elseif($_GET['hal'] == 'request-email'){
      include "halaman/email/request-email.php";
    }elseif($_GET['hal'] == 'setujui-request-email'){
      include "halaman/email/crud-request-email.php";
    }elseif($_GET['hal'] == 'tolak-request-email'){
      include "halaman/email/crud-request-email.php";
    }
    
    elseif($_GET['hal'] == 'email-di-proses'){
      include "halaman/email/email-di-proses.php";
    }elseif($_GET['hal'] == 'email-di-tolak'){
      include "halaman/email/email-di-tolak.php";
    }elseif($_GET['hal'] == 'edit-proses'){
      include "halaman/email/edit-proses.php";
    }elseif($_GET['hal'] == 'hapus-eamil-di-proses'){
      include "halaman/email/crud-email-di-proses.php";
    }
    

    elseif($_GET['hal'] == 'admin'){
      include "halaman/admin/admin.php";
    }elseif($_GET['hal'] == 'tambahedit-admin'){
      include "halaman/admin/tambahedit-admin.php";
    }elseif($_GET['hal'] == 'hapus-admin'){
      include "halaman/admin/crud.php";
    }

    elseif($_GET['hal'] == 'pesan'){
      include "halaman/pesan/pesan.php";
    }
    ?>

    <div class="footer">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <p>Hak Cipta &copy; 2019. UNIVERSITAS GUNADARMA</p>
          </div>
        </div>
      </div>
    </div>
    
    
    <a href="#" class="back-to-top"><span id="toTopHover" style="opacity: 1;"> </span></a>
    
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="assets/js/bootstrap.min.js"></script>

    <script src="http://cdn.ckeditor.com/4.5.10/full/ckeditor.js"></script>
    <script src="assets/js/ck-sample.js"></script>
    <script>
      initSample();
    </script>

    <!-- Own Js -->
    <script type="text/javascript" src="assets/js/own.js"></script>
  </body>
</html>