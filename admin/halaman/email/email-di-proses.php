<div class="main-content">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 content">
            <h3>Data Email Yang Sudah Di Proses</h3>
            <table class="table table-bordered">
              <thead>
                <th>No</th>
                <th>NIM</th>
                <th>Nama Lengkap</th>
                <th>Prodi</th>
                <th>Request Email</th>
                <th>Nomor Kontak</th>
                <th>Status Mahasiswa</th>
                <th>Email</th>
                <th>Tanggal Lahir</th>
                <th>Alamat</th>
                <th>Status</th>
                
              </thead>
              <?php
              $no=0;
              $query = mysqli_query($conn, "select * from registrasi where status_pendaftaran='Berhasil'");
              while ($array=mysqli_fetch_array($query)) {
              $no++;
              ?>
              <tbody>
                <td align="center"><?php echo $no ;?></td>
                <td align="center"><?php echo $array['nim'] ;?></td>
                <td align="center"><?php echo $array['nama_lengkap'] ;?></td>
                <td align="center"><?php echo $array['fakultas'] ;?></td>
                <td align="center"><?php echo $array['request_email'] ;?></td>
                <td align="center"><?php echo $array['nomor_kontak'] ;?></td>
                <td align="center"><?php echo $array['status_mahasiswa'] ;?></td>
                <td align="center"><?php echo $array['email'] ;?></td>
                <td align="center"><?php echo $array['tgl_lahir'] ;?></td>
                <td align="center"><?php echo $array['alamat'] ;?></td>
                <td align="center"><span class="label label-success">Sudah Di Proses</span></a></td>
                 
              <?php } ?>
            </table>
          </div>
        </div>
      </div>
    </div>