<div class="main-content">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 content">
            <h3>Pesan Masuk</h3>
            <table class="table table-bordered">
              <thead>
                <th>No</th>
                <th>Nama</th>
                <th>Email</th>
                <th>Pesan</th>
                <th>Tanggal Dikirim</th>
              </thead>
              <?php
              $no=0;
              $query = mysqli_query($conn,"select * from pesan");
              while ($array=mysqli_fetch_array($query)) {
              $no++;
              ?>
              <tbody>
                <td align="center"><?php echo $no;?></td>
                <td align="center"><?php echo $array['nama'];?></td>
                <td align="center"><?php echo $array['email'];?></td>
                <td align="center"><?php echo $array['pesan'];?></td>
                <td align="center"><?php echo $array['tgl'];?></td>
              </tbody>
              <?php } ?>
            </table>
          </div>
        </div>
      </div>
    </div>