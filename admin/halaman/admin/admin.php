
	<div class="main-content">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 content">
            <h3>Administrator</h3>
            <a href="media.php?hal=tambahedit-admin" class="btn btn-success mrgn-btm20"><i aria-hidden="true" class="fa fa-plus"></i> Tambah Baru</a>
            <table class="table table-bordered">
              <thead>
                <th>No</th>
                <th>Username</th>
                <th>Aksi</th>
              </thead>
              <?php
              	$no=0;
				$query = mysqli_query($conn, "select * from admin order by admin_id asc");
				while ($array = mysqli_fetch_array($query)) {
				$no++;
				?>
              <tbody>
                <td align="center"><?php echo $no;?></td>
                <td align="center"><?php echo $array['user'];?></td>
                <td align="center">
                  <a href="media.php?hal=hapus-admin&admin_id=<?php echo $array['admin_id'];?>" onclick="return confirm('Apakah anda yakin ?')"><span class="badge" title="Hapus"><i aria-hidden="true" class="fa fa-trash"></i></span></a>
                  <a href="media.php?hal=tambahedit-admin&admin_id=<?php echo $array['admin_id'];?>"><span class="badge" title="Edit"><i aria-hidden="true" class="fa fa-pencil"></i></span></a>
                </td>
              </tbody>
             <?php } ?>
            </table>
          </div>
        </div>
      </div>
    </div>