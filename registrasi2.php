<?php include "koneksi/koneksi.php" ;?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>REGISTRASI EMAIL</title>

    <!-- Favicon -->
    <link rel="shortcut icon" href="assets/assets/img/favicon.png">

    <!-- Bootstrap -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- Style -->
    <link href="assets/css/style.css" rel="stylesheet">
    <link href="assets/css/responsive.css" rel="stylesheet">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="assets/css/font-awesome.min.css">

    <!-- Font Google -->
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,900italic,900,700italic,700,500italic,500,400italic,300italic,300,100italic,100' rel='stylesheet' type='text/css'>

    <!-- Hover CSS -->
    <link href="assets/css/hover.css" rel="stylesheet" media="all">

    <!-- Slick Banner -->
    <link rel="stylesheet" type="text/css" href="assets/js/slick/slick.css"/>
    <link rel="stylesheet" type="text/css" href="assets/js/slick/slick-theme.css"/>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>
    <div class="main-menu default" id="navigation">
      <nav class="navbar navbar-default">
        <div class="container">
          <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.php"><img alt="logo" src="assets/img/logo-1.png" width="200px"></a>
          </div>

          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
              <!--<li><a href="index.html"><i aria-hidden="true" class="fa fa-home"></i> Beranda</a></li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i aria-hidden="true" class="fa fa-file-text-o"></i> Profil <span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><a href="#">Struktur Organisasi</a></li>
                  <li><a href="#">Visi & Misi</a></li>
                </ul>
              </li>
              <li><a href="#"><i aria-hidden="true" class="fa fa-newspaper-o"></i> Publikasi</a></li>
              <li><a href="#"><i aria-hidden="true" class="fa fa-phone"></i> Kontak</a></li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i aria-hidden="true" class="fa fa-photo"></i> Dokumentasi <span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><a href="#">Foto</a></li>
                  <li><a href="#">Video</a></li>
                </ul>
              </li>-->

              <li class="active"><a href="index.php">Beranda</a></li>
              <li><a href="registrasi.php">Registrasi</a></li>
              <li><a href="kontak.php">Kontak</a></li>
            </ul>
          </div>
        </div>
      </nav>
    </div>

    <div class="main-banner">
      <img src="assets/img/banner/struktur.png" class="img-responsive">
    </div>

    <div class="main-content">
      <div class="container">
        <div class="row">
          <div class="col-lg-6 col-lg-offset-3 mrgn-btm20">
            <div class="line-dropdown">
              <h4><span>Registrasi</span></h4>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-4 mrgn-btm40">
            <div class="sub-menu">
              <h5>Menu Registrasi</h5>
              <ul>
                <li><a href="registrasi.php">Daftar Request Email</a></li>
                <li class="active"><a href="registrasi2.php">Daftar Email Yang Sudah Di Proses</a></li>
                <li><a href="registrasi3.php">Daftar Email Yang Di Tolak</a></li>
              </ul>
            </div>
          </div>
          <div class="col-lg-8">
          <table class="table">
            <thead>
              <tr>
                <td align="center"><b>No</b></td>
                <td align="center"><b>NIM</b></td>
                <td align="center"><b>Nama Lengkap</b></td>
                <td align="center"><b>Prodi</b></td>
                <td align="center"><b>Student Mail</b></td>
              </tr>
            </thead>
            <tbody>
            <?php
            $no=0;
            $query = mysqli_query($conn,"select * from registrasi where status_pendaftaran='Berhasil'");
            while($array=mysqli_fetch_array($query)) {
            $no++;
            ?>
              <tr class="info">
                <td align="center"><?php echo $no;?></td>
                <td align="center"><?php echo $array['nim'];?></td>
                <td align="center"><?php echo $array['nama_lengkap'];?></td>
                <td align="center"><?php echo $array['fakultas'];?></td>
                <td align="center"><b><?php echo $array['request_email'];?></b></td>
              </tr>
            <?php } ?>
            </tbody>
          </table>
          </div>
        </div>
      </div>
    </div>

    <div class="footer">
      <div class="container">
        <div class="row">
          <div class="col-lg-8 col-lg-offset-2">
            <center><img alt="logo" src="assets/img/logo-1.png" class="img-responsive" width="200px"></center>
            <h5><i aria-hidden="true" class="fa fa-map-marker"></i>Jl. Margonda Raya 100, Depok West Java, INDONESIA - 16424<br><i aria-hidden="true" class="fa fa-envelope-o"></i> mediacenter [@] gunadarma.ac.id | <i aria-hidden="true" class="fa fa-phone"></i> +62 - 21 - 78881112 ext. 234</h5>
            <p>Hak Cipta &copy; 2019. UNIVERSITAS GUNADARMA</p>
          </div>
        </div>
      </div>
    </div>
    
    <a href="#" class="back-to-top"><span id="toTopHover" style="opacity: 1;"> </span></a>
    
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="assets/js/bootstrap.min.js"></script>

    <!-- Slick Banner -->
    <script type="text/javascript" src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
    <script type="text/javascript" src="assets/js/slick/slick.min.js"></script>

    <!-- Photo & Video Popup -->
    <script type="text/javascript" src="assets/js/jquery.fancybox.js?v=2.1.5"></script>
    <link rel="stylesheet" type="text/css" href="assets/css/jquery.fancybox.css?v=2.1.5" media="screen" />
    <script type="text/javascript" src="assets/js/jquery.fancybox-media.js?v=1.0.6"></script>
    <script type="text/javascript">
      $(document).ready(function() {
        $('.fancybox').fancybox();
        $('.fancybox-media')
          .attr('rel', 'media-gallery')
          .fancybox({
            openEffect : 'none',
            closeEffect : 'none',
            prevEffect : 'none',
            nextEffect : 'none',
            arrows : false,
            helpers : {
            media : {},
            buttons : {}
          }
        });
      });
    </script>

    <!-- Own Js -->
    <script type="text/javascript" src="assets/js/own.js"></script>

    <!-- Top Fixed Menu -->
    <script type="text/javascript" src="assets/js/nagging-menu.js" charset="utf-8"></script>
  </body>
</html>